// For now, don't care about light direction. 

varying vec4 diffuse;
//varying vec3 normal,lightDir,halfVector;
//varying float dist;


uniform vec4 right;
uniform vec4 up;
uniform vec3 size;


void main()
{	
	gl_TexCoord[0] = gl_MultiTexCoord0;

    float u = gl_MultiTexCoord0.s;
    float v = gl_MultiTexCoord0.t;

    /*
    vec4 pos = gl_ModelViewMatrix * gl_Vertex;
    pos += right * ((u-0.5) * size)
         + up * ((v-0.5) * size);  
    */

    vec4 pos = gl_Vertex + right * ((u-0.5) * size.x)
                         + up * ((v-0.5) * size.y);
   

	/* Compute the diffuse, ambient and globalAmbient terms */
    gl_FrontColor = gl_Color;

	diffuse = gl_FrontMaterial.diffuse;// * gl_LightSource[0].diffuse;

//    gl_Position = gl_ProjectionMatrix * pos;
    gl_Position = gl_ModelViewProjectionMatrix * pos;
}
