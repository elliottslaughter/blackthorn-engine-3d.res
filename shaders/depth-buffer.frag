// normalizing the depth so i can see it
uniform sampler2D tex;

const float zfar = 200.0;
const float znear = 0.2;
// Used to get the eye depth
float LinearizeDepth(float zb) {	
    float zn = zb * 2.0 - 1.0;
    return (2.0 * zfar * znear) 
         / (zfar + znear - zn*(zfar - znear));
//    return (2.0 * n) / (f + n - zoverw * (f - n));	
}

void main()
{
 // float depth = LinearizeDepth(texture2D(tex, gl_TexCoord[0].st).r)*1.0;
    vec3 texture = texture2D(tex, gl_TexCoord[0].st).xyz;
    float depth = (texture2D(tex, gl_TexCoord[0].st).r);

  gl_FragColor = vec4(texture, 1.0);
}
