// shadow skin shader
varying vec4 v_position;

// Joint matrices.  For now, we'll support a max of 32
// each vertex can have up to 4 joints. any more and you're
// being greedy.
uniform mat4 jointMats[32];

attribute vec4 jointIndices;
attribute vec4 jointWeights;

void main()
{	
	
	// Skinning
	vec4 pos = vec4(0.0, 0.0, 0.0, 1.0);
	ivec4 i_indices = ivec4( int( jointIndices[0] ), 
							 int( jointIndices[1] ),
							 int( jointIndices[2] ),
							 int( jointIndices[3] ) );
	// Joint 1	
	pos = (jointMats[i_indices[0]] * gl_Vertex) * jointWeights[0];
	
	// Joint 2
	pos = pos + (jointMats[i_indices[1]] * gl_Vertex) * jointWeights[1];
	
	// Joint 3
	pos = pos + (jointMats[i_indices[2]] * gl_Vertex) * jointWeights[2];
	
	// Joint 4
	pos = pos + (jointMats[i_indices[3]] * gl_Vertex) * jointWeights[3];

    gl_Position = gl_ModelViewProjectionMatrix * pos;
    v_position = gl_Position;
} 
