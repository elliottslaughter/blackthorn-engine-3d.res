varying vec4 diffuse,ambient;
varying vec3 normal,lightDir,halfVector;
varying float dist;

// Joint matrices.  For now, we'll support a max of 30
// each vertex can have up to 4 joints. any more and you're
// being greedy.
uniform mat4 jointMats[30];

attribute vec4 jointIndices;
attribute vec4 jointWeights;

void main()
{	

	gl_TexCoord[0] = gl_MultiTexCoord0;
	
	// Skinning
	vec4 pos = vec4(0.0, 0.0, 0.0, 1.0);
	normal = vec3(0.0);
	ivec4 i_indices = ivec4( int( jointIndices[0] ), 
							 int( jointIndices[1] ),
							 int( jointIndices[2] ),
							 int( jointIndices[3] ) );
	// Joint 1
	
	pos = (jointMats[i_indices[0]] * gl_Vertex) * jointWeights[0];
	normal = ((jointMats[i_indices[0]] * vec4(gl_Normal, 0.0)) * jointWeights[0]).xyz;
	
	// Joint 2
	pos = pos + (jointMats[i_indices[1]] * gl_Vertex) * jointWeights[1];
	normal = normal + ((jointMats[i_indices[1]] * vec4(gl_Normal, 0.0)) * jointWeights[1]).xyz;
	
	// Joint 3
	pos = pos + (jointMats[i_indices[2]] * gl_Vertex) * jointWeights[2];
	normal = normal + ((jointMats[i_indices[2]] * vec4(gl_Normal, 0.0)) * jointWeights[2]).xyz;
	
	// Joint 4
	pos = pos + (jointMats[i_indices[3]] * gl_Vertex) * jointWeights[3];
	normal = normal + ((jointMats[i_indices[3]] * vec4(gl_Normal, 0.0)) * jointWeights[3]).xyz;
	
   // pos = gl_Vertex;
   // normal = gl_Normal;
    
	/* first transform the normal into eye space and 
	   normalize the result */
	normal = normalize(gl_NormalMatrix * normal);
	
    // Find light vector for point light
    vec4 eye_pos = gl_ModelViewMatrix * pos;
    vec3 aux = vec3(gl_LightSource[0].position - eye_pos);
    dist = length(aux);
    
    // For a point light:
    lightDir = normalize(aux);

    /* Normalize the halfVector to pass it to the fragment shader */
    halfVector = normalize(gl_LightSource[0].halfVector.xyz);
				
    /* Compute the diffuse, ambient and globalAmbient terms */
    diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
    ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
    ambient += gl_LightModel.ambient * gl_FrontMaterial.ambient;

    gl_Position = gl_ModelViewProjectionMatrix * pos;
} 
