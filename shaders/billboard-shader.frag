// Fragment shader for billboard streams

uniform sampler2D tex;

varying vec4 diffuse;

//varying vec3 normal,lightDir,halfVector;
//varying float dist;

void main()
{
    vec4 color = texture2D(tex, gl_TexCoord[0].st);
	gl_FragColor = vec4(color.rgb / (1.0 - gl_Color.rgb), color.a*gl_Color.a);
	//gl_FragColor = vec4((1.0 - (1.0 - gl_Color.rgb) * (1.0 - color.rgb)), color.a*gl_Color.a);
}
