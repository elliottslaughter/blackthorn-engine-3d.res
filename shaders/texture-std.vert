// me too
varying vec4 diffuse,ambient;
varying vec3 normal,lightDir,halfVector;
varying float dist;
//varying vec4 shadowCoord;

uniform mat4 shadowMat;

void main()
{	

	// gl_MultiTexCoord is a pre-defined vertex attribute that
	// stores the texture coordinates of the vertex. gl_TexCoord[0]
	// is a pre-defined varying variable that is passed to the 
	// fragment shader.	
	gl_TexCoord[0] = gl_MultiTexCoord0;
 //   gl_TexCoord[5] = gl_MultiTexCoord0;

//    shadowCoord = shadowMat * (gl_ModelViewMatrix * gl_Vertex);
//	shadowCoord= gl_TextureMatrix[5] * (gl_ModelViewMatrix * gl_Vertex);
	
	/* first transform the normal into eye space and 
	normalize the result */
	normal = normalize(gl_NormalMatrix * gl_Normal);
	
	/* now normalize the light's direction. Note that 
	according to the OpenGL specification, the light 
	is stored in eye space. Also since we're talking about 
	a directional light, the position field is actually direction */
	//lightDir = -normalize(vec3(gl_LightSource[0].position));
    
    vec4 eye_pos = gl_ModelViewMatrix * gl_Vertex;
    vec3 aux = vec3(gl_LightSource[0].position - eye_pos);
    dist = length(aux);
    
    // For a point light:
    lightDir = normalize(aux);

	/* Normalize the halfVector to pass it to the fragment shader */
	halfVector = normalize(gl_LightSource[0].halfVector.xyz);
				
	/* Compute the diffuse, ambient and globalAmbient terms */
	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;
	ambient = gl_FrontMaterial.ambient * gl_LightSource[0].ambient;
	ambient += gl_LightModel.ambient * gl_FrontMaterial.ambient;
	
//    gl_Position = shadowCoord;
	gl_Position = ftransform();
} 
