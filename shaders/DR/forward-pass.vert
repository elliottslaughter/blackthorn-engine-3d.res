// Forward Rendering shader
// Vertex shader

varying vec4 diffuse, ambient;


void main () {

    ambient = gl_FrontMaterial.ambient;
    diffuse = gl_FrontMaterial.diffuse;
    
    vec4 pos = gl_ModelViewProjectionMatrix * gl_Vertex;
    
    gl_TexCoord[0] = vec4( (pos.xy*0.5 + 0.5), 0.0, 1.0);
    gl_Position = pos;
}
