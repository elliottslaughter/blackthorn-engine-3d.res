// Z-pass: store normal and depth information
// Vertex shader

varying vec3 normal;
varying vec4 viewPos;

void main () {    
    normal = normalize(gl_NormalMatrix * gl_Normal);
    viewPos = gl_ModelViewMatrix * gl_Vertex;
    gl_Position = gl_ProjectionMatrix * viewPos;
}
