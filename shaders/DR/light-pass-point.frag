// light-pass shader: point light
// Fragment shader

uniform sampler2D depthTex;
uniform sampler2D normalTex;

uniform float znear, 
              zfar;

varying vec3 eyeScreenRay;
//varying vec3 normal;
//varying vec3 l;


vec3 normal;
float depth;

// Used to get the eye depth
float LinearizeDepth(float zb) {	
    float zn = zb * 2.0 - 1.0;
    return (2.0 * zfar * znear) 
         / (zfar + znear - zn*(zfar - znear));
}

void main () {
    
    //depth = texture2D(depthTex, gl_TexCoord[0].st).r;    
    vec4 normalFetch = texture2D(normalTex, gl_TexCoord[0].st);
//    depth = LinearizeDepth( texture2D(depthTex, gl_TexCoord[0].st).r );
    normal = normalize(normalFetch.xyz*2.0 - 1.0);
    depth = normalFetch.w*200;
	
	//vec3 n = normalize(normal);

    vec3 pixelPos = normalize(eyeScreenRay) * depth;
    vec3 lightPos = gl_LightSource[0].position.xyz;    
    
    // Compute light attenuation and direction
    vec3 lightDir = lightPos - pixelPos;    // add att later
    lightDir = normalize(lightDir);

    // Phong
    //  float spec = pow(clamp( dot(reflect(normalize;    // do spec later
    
    // diffuse
    float NL = dot (lightDir, normal );
	//float NL = (
    float d2 = length(pixelPos);
    
 //   gl_FragColor = vec4( normal, 1.0);
  //  gl_FragColor = vec4(d2, d2, d2, 1.0);
  //  gl_FragColor = vec4( pixelPos, 1.0 );
    gl_FragColor = gl_LightSource[0].diffuse * NL;
}
