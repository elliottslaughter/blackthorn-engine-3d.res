// Z-pass: store normal and depth information
// Fragment shader

varying vec3 normal;
varying vec4 viewPos;

void main () {
    // Write the normal out to normal buffer.
    vec3 n = normalize(normal*0.5 + 0.5);
    gl_FragColor = vec4(n, viewPos.z/200.0);
}
