// Forward Rendering shader
// Fragment shader
uniform sampler2D normals;

varying vec4 ambient, diffuse;

void main () {
    vec4 light = texture2D(normals, gl_TexCoord[0].st);
//    vec3 NL = light.xyz;
    
    gl_FragColor = ambient + diffuse*light;
}
