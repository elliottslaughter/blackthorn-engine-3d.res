// light-pass shader: point light
// Vertex shader

varying vec3 eyeScreenRay;
//varying vec3 normal;
//varying vec3 l;

const float halffov = 0.7853981633; // pi/4, 45 degrees
const float aspect= 720.0/960.0;
const float zNear = 0.2;

void main () {
    float invTan = 1.0/tan(halffov);    // should be done not in the shader :P



    // don't have to do anything. we're drawing a screen quad.
    // we will need the screen coordinates though
    vec4 pos = gl_ModelViewProjectionMatrix * gl_Vertex;

//	normal = normalize(gl_NormalMatrix * gl_Normal);
//	l = vec3(gl_LightSource[0].position - pos);
	
    eyeScreenRay = vec3( pos.x, pos.y * aspect, invTan);
    //eyeScreenRay = vec3( 1.0, 1.0, 1.0 );

	//pos = gl_ProjectionMatrix * pos;
    gl_TexCoord[0] = vec4(pos.xy * 0.5 + 0.5, 0.0, 1.0);   // bias pos to [0 1] range
    gl_Position = pos;
}
