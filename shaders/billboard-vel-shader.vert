// For now, don't care about light direction. 
//attribute vec3 velocity;


varying vec4 diffuse;
//varying vec3 normal,lightDir,halfVector;
//varying float dist;


//uniform vec4 right;
//uniform vec4 up; // the particle's velocity will be the 'up' direction
uniform vec3 size;

void main()
{	
	gl_TexCoord[0] = gl_MultiTexCoord0;

    float u = gl_MultiTexCoord0.s;
    float v = gl_MultiTexCoord0.t;

  // vec3 vel = vec3((gl_ModelViewMatrix * vec4(velocity, 0.0)).xy, 0.0 );
    vec3 vel = (gl_ModelViewMatrix * vec4(gl_Normal, 0.0)).xyz;
    vec3 pos = (gl_ModelViewMatrix * gl_Vertex).xyz;
    vec3 norm_v = normalize(vel);
    vec3 right = normalize( cross(norm_v, vec3(0.0, 0.0, 1.0)) );

    pos += right * ((u-0.5) * size.x)
         + (norm_v*(v-0.5))*size.y + vel*v*size.z;
   

	/* Compute the diffuse, ambient and globalAmbient terms */
    gl_FrontColor = gl_Color;

	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;

//    gl_Position = gl_ProjectionMatrix * pos;
    gl_Position = gl_ProjectionMatrix * vec4(pos, 1.0);
}
