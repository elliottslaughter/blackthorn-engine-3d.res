// i'm a shader
uniform sampler2D tex;
uniform sampler2DShadow shadow;

//varying vec4 shadowCoord;

varying vec4 diffuse,ambient;
varying vec3 normal,lightDir,halfVector;
varying float dist;



float LinearizeDepth(float zoverw) {	
    float n = 0.1;
    float f = 100.0;		
    return (2.0 * n) / (f + n - zoverw * (f - n));	
}

void main()
{
	vec3 n,halfV;
	float NdotL,NdotHV,att;

    // shadow mapping
   // const float bias = 0.2;
   // vec3 shadow_w = shadowCoord.xyz / shadowCoord.w;
   // float light_depth = shadow2DProj(shadow, shadowCoord).r + bias;
                        //LinearizeDepth(texture2DProj(shadow, shadowCoord).r) + bias;
   // float cam_depth = shadow_w.z;//LinearizeDepth(shadow_w.z);

    //light_depth =  (cam_depth <= light_depth) ? 1.0 : 0.0;
	
	/* The ambient term will always be present */
	vec4 color = vec4(0.0);//= ambient;

	/* a fragment shader can't write a varying variable, hence we need
	a new variable to store the normalized interpolated normal */
	n = normalize(normal);
	
	/* compute the dot product between normal and ldir */
	NdotL = max(dot(n,lightDir),0.0);

	if (NdotL > 0.0) {
        att = 1.0 / (gl_LightSource[0].constantAttenuation + 
                     gl_LightSource[0].linearAttenuation * dist + 
                     gl_LightSource[0].quadraticAttenuation * dist * dist);
		color += diffuse * NdotL;
		halfV = normalize(halfVector);
		NdotHV = max(dot(n,halfV),0.0);
		color += att * gl_FrontMaterial.specular * 
			 	 gl_LightSource[0].specular * 
				 pow(NdotHV, gl_FrontMaterial.shininess);
	}

    
	gl_FragColor = (color + ambient) * texture2D(tex, gl_TexCoord[0].st);

   // gl_FragColor = vec4(1.0);//texture2D(shadow, gl_TexCoord[0].st);

}
